let currentSlide = 1;
const totalSlides = 3;

function init() {

	inputs = document.getElementsByTagName("input");
	for (i = 0; i < inputs.length; i++) {
		if(i>0) {
			inputs[i].checked=false; 
		} else {
			inputs[i].checked=true; 
		}
	}
}

function resetSlider() {

	for(let i=0; i<totalSlides; i++) {
		document.getElementById('slide'+(i+1)).style.display="none";
	}

}

function changeSlide() {

	let num = currentSlide;

	resetSlider();

	let current = document.getElementById('slide'+(num));
	current.style.display = "block";

}

function displaySlide1() {
	currentSlide=1;
	changeSlide();
}

function displaySlide2() {
	currentSlide=2;
	changeSlide();
}

function displaySlide3() {
	currentSlide=3;
	changeSlide();
}

init();
